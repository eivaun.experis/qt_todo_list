#ifndef TASK_H
#define TASK_H

#include <QWidget>
#include <QString>
#include <QDateTime>
#include <QTimer>

namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task(QWidget *parent = 0);
    ~Task();

    bool isCompleted() const;

    void setName(const QString& newName);
    QString getName() const;

    void setDescription(const QString& newDescription);
    QString getDescription() const;

    void setDeadline(const QDateTime& newDeadline);
    QDateTime getDeadline() const;

public slots:
    void edit();

signals:
    void removed(Task* task);
    void statusChanged(Task* task);

private slots:
    void checked(bool checked);
    void updateTimeLeft();

private:
    Ui::Task *ui;
    QString name;
    QString description;
    QDateTime deadline;
    inline static QTimer updateTimer = QTimer(nullptr);
    QString timeLeftLabelColorStyle = QString("QLabel{ color: %1; }");
    constexpr static QColor timeLeftLabelColorOverdue = QColor(255, 0, 0);
};

#endif // TASK_H
