#include "EditTaskDialog.h"
#include "ui_EditTaskDialog.h"

EditTaskDialog::EditTaskDialog(QWidget *parent, EditTaskDialogMode mode) : QDialog(parent), ui(new Ui::CreateTaskDialog), mode(mode)
{
    ui->setupUi(this);

    if(mode == EditTaskDialogMode::New)
    {
        ui->titleLabel->setText("New task");
        acceptButton = ui->buttonBox->addButton(QDialogButtonBox::Ok);
    }
    else if(mode == EditTaskDialogMode::Edit)
    {
        ui->titleLabel->setText("Edit task");
        acceptButton = ui->buttonBox->addButton(QDialogButtonBox::Save);
    }
    setWindowTitle(ui->titleLabel->text());

    ui->taskDeadlineInput->setDateTime(QDateTime::currentDateTime());
    on_taskNameInput_textChanged(ui->taskNameInput->text());


}

EditTaskDialog::EditTaskDialog(QWidget *parent, EditTaskDialogMode mode, QString name, QString description, QDateTime deadline) : EditTaskDialog(parent, mode)
{
    ui->taskNameInput->setText(name);
    ui->taskDescriptionInput->setPlainText(description);
    ui->taskDeadlineInput->setDateTime(deadline);
}

EditTaskDialog::~EditTaskDialog()
{
    delete ui;
}

QString EditTaskDialog::getName() const
{
    return ui->taskNameInput->text();
}

QString EditTaskDialog::getDescription() const
{
    return ui->taskDescriptionInput->document()->toPlainText();
}

QDateTime EditTaskDialog::getDeadline() const
{
    return ui->taskDeadlineInput->dateTime();
}

void EditTaskDialog::on_taskNameInput_textChanged(const QString &text)
{
    acceptButton->setDisabled(text.isEmpty());
}

