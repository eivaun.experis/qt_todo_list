#pragma once

#include <QDialog>
#include <QString>
#include <QPushButton>

namespace Ui {
class CreateTaskDialog;
}

enum class EditTaskDialogMode
{
    New, Edit
};

class EditTaskDialog : public QDialog
{
    Q_OBJECT

public:
    EditTaskDialog(QWidget *parent, EditTaskDialogMode mode);
    EditTaskDialog(QWidget *parent, EditTaskDialogMode mode, QString name, QString description, QDateTime deadline);
    ~EditTaskDialog();

    QString getName() const;
    QString getDescription() const;
    QDateTime getDeadline() const;

private slots:
    void on_taskNameInput_textChanged(const QString &text);

private:
    Ui::CreateTaskDialog *ui;
    EditTaskDialogMode mode;
    QPushButton* acceptButton;
};

