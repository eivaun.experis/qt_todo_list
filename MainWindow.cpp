#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "EditTaskDialog.h"

#include <QDebug>
#include <QInputDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    updateStatus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    EditTaskDialog dialog(this, EditTaskDialogMode::New);
    QDialog::DialogCode result = static_cast<QDialog::DialogCode>(dialog.exec());

    if (result == QDialog::DialogCode::Accepted)
    {
        qDebug() << "Adding new task";
        Task* task = new Task();
        task->setName(dialog.getName());
        task->setDescription(dialog.getDescription());
        task->setDeadline(dialog.getDeadline());
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    for(auto t : mTasks)  {
        if (t->isCompleted()) {
            completedCount++;
        }
    }
    int todoCount = mTasks.size() - completedCount;

    ui->statusLabel->setText(QString("Status: %1 todo / %2 completed")
                                 .arg(todoCount)
                                 .arg(completedCount));
}
