#include "EditTaskDialog.h"
#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>

Task::Task(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Task)
{
    ui->setupUi(this);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::edit);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);

    connect(&updateTimer, &QTimer::timeout, this, &Task::updateTimeLeft);

    if(!updateTimer.isActive()) { updateTimer.start(1000); }
}

Task::~Task()
{
    qDebug() << "~Task() called";
    disconnect(&updateTimer, &QTimer::timeout, this, &Task::updateTimeLeft);
    delete ui;
}

bool Task::isCompleted() const
{
    return ui->checkbox->isChecked();
}

void Task::setName(const QString& newName)
{
    name = newName;
    ui->checkbox->setText(newName);
}

QString Task::getName() const
{
    return name;
}

void Task::setDescription(const QString &newDescription)
{
    description = newDescription;
    if(description.isEmpty())
    {
        ui->descriptionLabel->hide();
    }
    else
    {
        ui->descriptionLabel->setText(newDescription);
        ui->descriptionLabel->show();
    }
}


QString Task::getDescription() const
{
    return description;
}

void Task::setDeadline(const QDateTime &newDeadline)
{
    deadline = newDeadline;
    updateTimeLeft();
}

QDateTime Task::getDeadline() const
{
    return deadline;
}

void Task::edit()
{
    EditTaskDialog dialog(this, EditTaskDialogMode::Edit, name, description, deadline);
    QDialog::DialogCode result = static_cast<QDialog::DialogCode>(dialog.exec());

    if (result == QDialog::DialogCode::Accepted)
    {
        setName(dialog.getName());
        setDescription(dialog.getDescription());
        setDeadline(dialog.getDeadline());
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    if(checked)
    {
        ui->descriptionLabel->hide();
        ui->timeLeftLabel->hide();
    }
    else
    {
        ui->descriptionLabel->show();
        ui->timeLeftLabel->show();
    }

    emit statusChanged(this);
}

void Task::updateTimeLeft()
{
    auto now = QDateTime::currentDateTime();

    auto days = abs(now.daysTo(deadline));
    auto secs = now.secsTo(deadline);
    bool overdue = secs < 0;
    secs = abs(secs);

    QString text = overdue ? "Time overdue: %1 %2" : "Time left: %1 %2";
    if(days > 0)
        ui->timeLeftLabel->setText(text.arg(days).arg("days"));
    else if (secs > 3600)
        ui->timeLeftLabel->setText(text.arg(secs / 3600).arg("hours"));
    else if (secs > 60)
        ui->timeLeftLabel->setText(text.arg(secs / 60).arg("minutes"));
    else
        ui->timeLeftLabel->setText(text.arg(secs).arg("seconds"));

    if(overdue)
        ui->timeLeftLabel->setStyleSheet(timeLeftLabelColorStyle.arg(timeLeftLabelColorOverdue.name()));
    else
        ui->timeLeftLabel->setStyleSheet("");
}
